# turr3t

A little app with portal gifs || sounds.  

## Install

Download repo and copy over to `apps`.  

the samples folder need to be copied to the root sys folder where there are already the integrated wav files.  
In Version to it should copy the files by it self if the dont exist.  

## Warning

The first start takes a bit.  
wait for moving pictures and be amazed (pwease).

## Credits

Frankstein code from [chubbson](https://git.flow3r.garden/chubbson/nyan-cat) || [rorist](https://git.flow3r.garden/rorist/endless-sequencer).  
thank u <3  

sounds from [portal wiki](https://theportalwiki.com/wiki/category:Turret_voice_files)
